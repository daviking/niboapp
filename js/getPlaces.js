var map, latlngbounds;

function getCountyStores(){
  var randomCode = document.getElementById("search_input").value;
  if (randomCode == " ") {
    alert("Enter a valid nibo code");
  }else {
      var xmlHttpNiboCode = new XMLHttpRequest();
      var url = "http://localhost/nibolywebapp/php/getfromdb.php?randcode=" + randomCode + "&lat=&lng=";
      xmlHttpNiboCode.open("GET", url, false);
      xmlHttpNiboCode.send();
        if (xmlHttpNiboCode.readyState == 4 && xmlHttpNiboCode.status == 200) {
          var result = xmlHttpNiboCode.responseText;
          drawMap(result);
          // alert(result);
        }else  {
          alert("Could not get the required information");
          }
        }
      }
// END getCountyStores function

function getZipStores() {
    var lat, lng;
    var zipCode = document.getElementById("search_input").value;
    if (zipCode != "") {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({componentRestrictions: {country: 'NG',postalCode: zipCode}
        }, function(results, status){
          if(status == google.maps.GeocoderStatus.OK)
            {if (results){
              lat = results[0].geometry.location.lat();
              lng = results[0].geometry.location.lng();
              var xmlHttpZip = new XMLHttpRequest();
              var urlZip = "http://localhost/getfromdb.php?randcode=&lat=" + lat + "&lng=" + lng;
              xmlHttpZip.open("GET", urlZip, false);
              xmlHttpZip.send();
              if (xmlHttpZip.readyState == 4 && xmlHttpZip.status == 200){
                var result = xmlHttpZip.responseText;
                drawMap(result);
                  } else {
                      alert("Could not get the required information!!!");
                    }
                  }
                }
                else {
                  alert("Entered zipcode is not valid!!!");
                }
              });
            }
            else {
              alert("Enter a zip code");
            }
          }


// drawMap function
function drawMap(result) {

    var arr = result.split("<br />");
    if (arr[0] == "0.00") {
      alert("There is no store near the specified post code!!!");
    }

var center = arr[0].split(":");
var latitude = center[0];
var longitude = center[1];
centerLocation = new google.maps.LatLng(latitude, longitude);

var mapOptions = {
  center: centerLocation,
  zoom: 11,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};

map = new google.maps.Map(document.getElementById("interactive"), mapOptions);

latlngbounds = new google.maps.LatLngBounds();

for(var i=0;i<arr.length-1;i++)  {

drawMarker(arr[i]);

}

map.fitBounds(latlngbounds);

}
// end drawMap function


// drawMarker function
function drawMarker(store) {
  var individual = store.split(":");
  var location = new google.maps.LatLng(individual[0], individual[1]);
  marker = new google.maps.Marker({position: location,map:map});
  latlngbounds.extend(marker.position);
  var infoWindow = new google.maps.InfoWindow({
    content: "<b>Store:</b> " + individual[2] + "<br /><b>Address:</b> " +
    individual[3] + "<br /><b>Nibo Code:</b> " + individual[4]
  });
  google.maps.event.addListener(marker, 'click', function(){
    infoWindow.open(map, this)
  });
  }

// end drawMarker
