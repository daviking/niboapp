var watchId, geocoder, startLat, startLong;
var start = 1;
window.onload = function() {
if (navigator.geolocation) {
  watchId = navigator.geolocation.getCurrentPosition(onSuccess, onError,
  {
    // maximumAge: 60*1000,
    // timeout: 5*60*1000,
    // enableHighAccuracy:true
  });
}
}

function onSuccess(position) {
var currentLat = position.coords.latitude;
var currentLong = position.coords.longitude;

if (start == 1) {
  startLat = currentLat;
  startLong = currentLong;
  start = 0;
}

var geocoder = new google.maps.Geocoder();
var latlong = new google.maps.LatLng(currentLat, currentLong);

geocoder.geocode({'latlng' : latlong}, function (result, status) {
  if (status == google.maps.GeocoderStatus.OK) {
    if (results){
      // alert(result[0].formatted_address);
        // document.getElementById("location").innerHTML = "You are now near" + result[0].formatted_address;
    }
  } else {
    alert ("Could not get Geolocation Information");
  }
});

// document.getElementById("location").innerHTML = currentLat + " " + currentLong;

var mapOptions = {
  center: new google.maps.LatLng(startLat, startLong),
  zoom: 15,
  mapTypeId: google.maps.MapTypeId.HYBRID
};

var mappy = new google.maps.Map(document.getElementById("interactive"), mapOptions);

var marker = new google.maps.Marker({
  position: latlong,
  map: mappy,
  title: "This is Your Position",
  animation: google.maps.Animation.BOUNCE, //or you use DROP
  // icon: "green.png",
  //using your own icon to show as marker
});
}

function onError(error) {
switch (error.code) {
  case PERMISSION_DENIED: alert("User denied permission");
  break;
  case TIMEOUT: alert("Geolocation timed out");
  break;
  case POSITION_UNAVAILABLE: alert("Geolocation information is not available");
  break;
  default: alert("Unknown error");
  break;

}
}
