var points = [{}, {}];

var map;

function findPath() {
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(onSuccess, onError,{maximumAge:60*1000, timeout:5*60*1000, enableHighAccuracy:true});
    }else
      document.getElementById("mapShower").innerHTML = "Your browser does not support HTML5 Geolocation!!!";
    }

function onSuccess(position) {
  points[0].lat = position.coords.latitude; // latitude source
  points[0].long = position.coords.longitude; // longitude source
  var localAddress = document.getElementById("destination").value.replace(" ", "+"); // getting the Destination address from the input
  var xmlhttpAddr = new XMLHttpRequest();
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + localAddress; // adding it to google geocode address so that it can look like a real address and it is saved to the url variable.
  xmlhttpAddr.open("GET", url, false);
  xmlhttpAddr.send();
  if (xmlhttpAddr.readyState == 4 && xmlhttpAddr.status == 200) {
    var result = xmlhttpAddr.responseText;
    var jsResult = eval("(" + result + ")");
    points[1].lat = jsResult.results[0].geometry.location.lat;
    points[1].long = jsResult.results[0].geometry.location.lng;
  }

var mapOptions = {
  center: new google.maps.LatLng(points[0].lat, points[0].long),
  zoom: 10,
  mapTypeId: google.maps.MapTypeId.ROADMAP
  };

map = new google.maps.Map(document.getElementById("mapShower"), mapOptions);

var latlngbounds = new google.maps.LatLngBounds();

for(var i=0;i<points.length;i++) {
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(points[i].lat, points[i].long),
    map:map
  });

  latlngbounds.extend(marker.position);
  }
  map.fitBounds(latlngbounds); // To zoom maps according to the source and destination points.
  drawPath();
}

function drawPath() {
  var directionsService = new google.maps.DirectionsService();
  var poly = new google.maps.Polyline({strokeColor:"#FF0000", strokeWeight:4});
  var request = {
    origin: new google.maps.LatLng(points[0].lat, points[0].long),
    destination: new google.maps.LatLng(points[1].lat, points[1].long),
    travelMode: google.maps.DirectionsTravelMode.DRIVING
  };
  directionsService.route(request, function(response, status){
    if (status == google.maps.DirectionsStatus.OK)
    {
      new google.maps.DirectionsRenderer({
        map:map,
        polylineOptions: poly,
        directions:response
      });
    }
  });
  }

function onError(error) {

switch(error.code) {

case PERMISSION_DENIED: alert("User denied permission");
break;

case TIMEOUT: alert("Geolocation timed out");
break;

case POSITION_UNAVAILABLE: alert("Geolocation information is not available");
break;

default: alert("Unknown error");
break;

  }

}
