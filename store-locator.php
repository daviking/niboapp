
<!DOCTYPE html>
<html>

<head>

<title>Store Locator</title>



<style>

fieldset{

width:250px;

display: inline;

}

</style>

<script type="text/javascript">

var map, latlngbounds;

function getCountyStores()

{

var county = document.getElementById("county").value;

if (county == "nothing")

{

alert("Select a valid county");

}

else

{

var xmlHttpCounty = new XMLHttpRequest();

var url = "http://localhost/getfromdb.php?county=" + county + "&lat=&lng=";
// var url = "http://test.gbacard.com/test_nibo/get_house_location.php?";
alert (url);

xmlHttpCounty.open("GET", url, false);

xmlHttpCounty.send();

if (xmlHttpCounty.readyState == 4 && xmlHttpCounty.status == 200)

{

var result = xmlHttpCounty.responseText;

drawMap(result);

}

else

{

alert("Could not get the required information");

}

}

}

function getZipStores()

{

var lat, lng;

var zipCode = document.getElementById("zip").value;

if (zipCode != "")

{

var geocoder = new google.maps.Geocoder();

geocoder.geocode({

componentRestrictions: {

country: 'NG',

postalCode: zipCode

}

}, function(results, status){

if(status == google.maps.GeocoderStatus.OK)

{

if (results)

{

lat = results[0].geometry.location.lat();

lng = results[0].geometry.location.lng();

var xmlHttpZip = new XMLHttpRequest();

var urlZip = "http://localhost/getfromdb.php?county=&lat=" + lat + "&lng=" + lng;
alert(urlZip);
xmlHttpZip.open("GET", urlZip, false);

xmlHttpZip.send();

if (xmlHttpZip.readyState == 4 && xmlHttpZip.status == 200)

{

var result = xmlHttpZip.responseText;

drawMap(result);

}

else

{

alert("Could not get the required information!!!");

}

}

}

else

{

alert("Entered zipcode is not valid!!!");

}

});

}

else

{

alert("Enter a zip code");

}

}

function drawMap(result)

{

var arr = result.split("<br />");

if (arr[0] == "0.00")

{

alert("There is no store near the specified post code!!!");

}

var center = arr[0].split(":");

var latitude = center[0];

var longitude = center[1];

centerLocation = new google.maps.LatLng(latitude, longitude);

var mapOptions = {

center: centerLocation,

zoom: 11,

mapTypeId: google.maps.MapTypeId.ROADMAP

};

map = new google.maps.Map(document.getElementById("mapArea"), mapOptions);

latlngbounds = new google.maps.LatLngBounds();

for(var i=0;i<arr.length-1;i++)

{

drawMarker(arr[i]);

}

map.fitBounds(latlngbounds);

}

function drawMarker(store)

{

var individual = store.split(":");

var location = new google.maps.LatLng(individual[0], individual[1]);

marker = new google.maps.Marker({

position: location,

map:map

});

latlngbounds.extend(marker.position);

var infoWindow = new google.maps.InfoWindow({

content: "<b>Store:</b> " + individual[2] + "<br /><b>Address:</b> " +

individual[3] + "<br /><b>Zip:</b> " + individual[4]

});

google.maps.event.addListener(marker, 'click', function(){

infoWindow.open(map, this)

});

}

</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8sTgqTPooq66mD0KTEaD79rQV4skzGHI"></script>

</head>

<body>

<fieldset>

<legend><b>Stores within a selected County</b></legend>

County:

<select id="county" name="county" onchange="getCountyStores();">

<option value="nothing">Select</option>

<?php

$connection = mysqli_connect("localhost", "root", "", "niboly") or die("Problem with connection!!");

// mysqli_select_db($connection, "niboly");

$query = mysqli_query($connection, "SELECT DISTINCT county FROM markers");

while($row = mysqli_fetch_array($query))

{

echo "<option value=" . $row['county'] .">" . $row['county'] . "</option>";

}

mysqli_close($connection);

?>

</select>

</fieldset> OR

<fieldset>

<legend><b>Stores near a specific PostCode</b></legend>

Postcode: <input type="text" id="zip" name="zip" />

<button id="findStore" onclick="getZipStores();">Find</button>

</fieldset><br />

<div id="mapArea" style="width:500px;height:500px"></div>

</body>

</html>
